package main

import (
	"fmt"
	"net"
	"os"
	"sync"
	"strings"
	"time"
	"math/rand"
	"math"
	"runtime"
	"container/list"
	"strconv"
	"hash/crc32"
	"bufio"
)

var (
	//parameters need to communicate with server
	SERVER_IP string = "206.87.122.190"
	SERVER_PORT string = "5000"
	PASSWORD string = "password"  // for security	
	MASTER_IP string
	MASTER_PORT string
	//local Ip and Port
	localIp string
	port string
	peerList = list.New()
	//locks
	mutexPeerList sync.Mutex
	mutexLocalTime sync.Mutex
	
	/*** clock synchronization ***/	
	state string
	localTime uint64 = 0
	clientId uint32
	deltaMap map[string]*Timedelta
	sequenceId uint64 = 1
	sem chan int
	masterAlive chan bool
	acceptFirst chan bool
	electionch chan bool
	acceptch chan bool
	masterupForAccept chan bool
	masterupForSlave chan bool
	slaveupForMasterup chan bool
	SYNCINTERVAL int64 = 5000
	CLICKINTERVAL int64 = 1000
	DRIFT int64
	
	//timeout settings
	TIMEOUTSTARTUP int64 = 1000
	TIMEOUTREQUESTTIME int64 = 10000
	TIMEOUTVALUEMAX int64 = 20000
	TIMEOUTVALUEMIN int64 = 10000
	TIMEOUTCANDIDATE int64 = 10000
	TIMEOUTACCEPT int64 = 10000
	MAXDELTA int64 = 10
		
	//others
 	BUFFERSIZE int = 1024
	quit bool = false
	stop bool = false
	
)
const (
	/*** clock synchronization ***/
	// All the possible states
	STARTUP string = "STARTUP"
//	NOMASTER string = "NOMASTER"
	MASTER string = "MASTER"
	ACCEPT string = "ACCEPT"
	CANDIDATE string = "CANDIDATE"
	SLAVE string = "SLAVE"
	
	//message types
	MASTERREQ string = "Masterreq"
	MASTERSETUP string = "Mastersetup"
	HAVEMASTER string = "Havemaster"
	NOMASTER string = "Nomaster"
	MASTERSETUPACK string = "Mastersetupack"
	PEERLIST string = "Peerlist"
	REQUESTTIME string = "Requesttime"
	CORRECTTIME string = "Correcttime"
	LOCALTIME string = "Localtime"
	ELECTION string = "Election"
	REFUSE string = "Refuse"
	ACCEPTACK string = "Acceptack"
	MASTERUP string = "Masterup"
	SLAVEUP string = "Slaveup"
	// Different mode
	debug bool = true
	info bool = true
	enableSync bool = true
)

type Peer struct {
		conn net.Conn
		port string
		ip string
	}
type Timedelta struct {
	atSend uint64
	atReceive uint64
	atSlave uint64
	delta int64
}
func main() {
	localIp = getLocalIP()[0]
	port = generatePortNo()
	clientId = hash(localIp + ":" + port)
	if debug { fmt.Println("Client ID is ", clientId, strconv.FormatUint(uint64(clientId), 10), strconv.FormatUint(uint64(clientId), 16)) } 
	//peerList := list.New()
	state = STARTUP
	getMasterFromServer()
	go localTimeClick()
	runtime.Gosched()
	buildPeerList()
	go clockSync()
	runtime.Gosched()
	for {
		time.Sleep(10000 * time.Millisecond)
	}
}
func getLocalTime() uint64 {
	return localTime
}
func localTimeClick() {
	rand.Seed(time.Now().Unix())
	DRIFT = rand.Int63n(CLICKINTERVAL/2) - CLICKINTERVAL / 4
	for {
		time.Sleep(time.Duration(CLICKINTERVAL + DRIFT) * time.Millisecond)
		mutexLocalTime.Lock()
		localTime++;
		mutexLocalTime.Unlock()
	}
}
func getMasterFromServer() {
	tcpAddr, err := net.ResolveTCPAddr("tcp", SERVER_IP + ":" + SERVER_PORT)
	if info { fmt.Println("Connecting to server at " + SERVER_IP + ":" + SERVER_PORT) }
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	errMsg := "Cannot connect to server at " + SERVER_IP + ":" + SERVER_PORT
	checkError(err, errMsg)
	var request string = MASTERREQ
	var response string
	defer conn.Close()
	for {
		if info { fmt.Println("Send request to server :", request)}
		_, err = conn.Write([]byte(request))
		errMsg := "Cannot send request to server at " + SERVER_IP + ":" + SERVER_PORT
		checkError(err, errMsg)
		timeout := time.NewTimer(time.Duration(TIMEOUTSTARTUP) * time.Millisecond)
		ch := make(chan string)
		
		go func() {
			buf := make([]byte, BUFFERSIZE)
			n, err := conn.Read(buf[0:])
			errMsg := "Cannot get response from server at " + SERVER_IP + ":" + SERVER_PORT
			checkError(err, errMsg)
			ch <- string(buf[0:n])
		}()
		select {
			case response = <- ch:
				break
			case <- timeout.C:
				fmt.Println("Time out. Cannot connect to server. Try again.")
				break
		}
		if debug { fmt.Println("Get reply from server :", response)}
		tokens := strings.Split(response, ":")
		if strings.EqualFold(tokens[0], HAVEMASTER) {
			MASTER_IP = tokens[1]
			MASTER_PORT = tokens[2]
			state = SLAVE
			break
		} else if strings.EqualFold(tokens[0], NOMASTER) {
			request = MASTERSETUP + ":" + localIp + ":" + port
		} else if strings.EqualFold(tokens[0], MASTERSETUPACK) {
			state = MASTER
			MASTER_IP = localIp
			MASTER_PORT = port
			break
		} 			
	}
}
func buildPeerList() {
	if strings.EqualFold(state, MASTER) {
		go acceptPeers(port, peerList)
		runtime.Gosched()
	} else if strings.EqualFold(state, SLAVE) {
		connectToIpPort(MASTER_IP + ":" + MASTER_PORT, peerList)
		go acceptPeers(port, peerList)
		runtime.Gosched()
	} else {
		if debug { fmt.Println("Should not reach here, fatal error!")}
	}	
}
func masterSetUpToServer() {
	tcpAddr, err := net.ResolveTCPAddr("tcp", SERVER_IP + ":" + SERVER_PORT)
	if info { fmt.Println("Connecting to server at " + SERVER_IP + ":" + SERVER_PORT) }
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	errMsg := "Cannot connect to server at " + SERVER_IP + ":" + SERVER_PORT
	checkError(err, errMsg)
	var request string = MASTERSETUP + ":" + localIp + ":" + port
	var response string
	defer conn.Close()
	for {
		if info { fmt.Println("Send request to server :", request)}
		_, err = conn.Write([]byte(request))
		errMsg := "Cannot send request to server at " + SERVER_IP + ":" + SERVER_PORT
		checkError(err, errMsg)
		timeout := time.NewTimer(time.Duration(TIMEOUTSTARTUP) * time.Millisecond)
		ch := make(chan string)
		
		go func() {
			buf := make([]byte, BUFFERSIZE)
			n, err := conn.Read(buf[0:])
			errMsg := "Cannot get response from server at " + SERVER_IP + ":" + SERVER_PORT
			checkError(err, errMsg)
			ch <- string(buf[0:n])
		}()
		select {
			case response = <- ch:
				break
			case <- timeout.C:
				fmt.Println("Time out. Cannot connect to server. Try again.")
				break
		}
		if debug { fmt.Println("Get reply from server :", response)}
		tokens := strings.Split(response, ":")
		if strings.EqualFold(tokens[0], MASTERSETUPACK) {
			state = MASTER
			MASTER_IP = localIp
			MASTER_PORT = port
			break
		} 			
	}
}
func clockSync() {
	for {
		switch state {				
			case MASTER:
				if info { fmt.Println("I am master") }	
				if debug { fmt.Println(MASTER_IP, MASTER_PORT) }
				if enableSync {
					sequenceId = 1
					deltaMap = make(map[string]*Timedelta)
				//	go chatSay(peerList) for shuochen
				//	runtime.Gosched()
					for !quit {
						time.Sleep(time.Duration(SYNCINTERVAL) * time.Millisecond)
						deltaMap = make(map[string]*Timedelta)
						mutexPeerList.Lock()
						numberOfPeers := peerList.Len()
						mutexPeerList.Unlock()
						if debug { fmt.Println("Number of peers", numberOfPeers)}
						sem = make(chan int, numberOfPeers)
						requestTime(peerList, sequenceId) 
					
						timeout := time.NewTimer(time.Duration(TIMEOUTREQUESTTIME) * time.Millisecond)
						ch := make(chan string)
						go func() {
							for i:= 0; i < numberOfPeers ; i++ {
								<- sem
							}
							ch <- "Finished"
						}()
						select {
							case <- ch:
								if debug { fmt.Println("All localtime have been received!") }
							case <- timeout.C:
								if debug { fmt.Println("Some of localtime have been received!") }
						}
						var num int64 = 0
						var average int64 = 0
						for k, v := range deltaMap {
							v.delta = int64((v.atSend + v.atReceive)/2 - v.atSlave)
							if math.Abs(float64(v.delta)) < float64(MAXDELTA) {
								num++
								average += v.delta
								if debug { fmt.Println(k, v.atSend, v.atReceive, v.atSlave, v.delta) }
							}
						}
					
						average = average / (num+1)
						if debug { fmt.Println("average is" , average) }
						mutexLocalTime.Lock()
				
						if average < 0 {
							localTime = localTime + uint64(-average)
						} else {
							localTime = localTime - uint64(average)
						}				
						mutexLocalTime.Unlock()
						correctTime(peerList, sequenceId, average)
						sequenceId++
					}
				}
				for {
					time.Sleep(1000*time.Millisecond)
				}
				break
			case SLAVE:
				if info { fmt.Println("I am slave") }
			//	fmt.Println(MASTER_IP, MASTER_PORT)
			//	go chatSay(peerList)
				masterAlive = make(chan bool)
				acceptFirst = make(chan bool)
				masterupForSlave = make(chan bool)
				timeoutSlave := rand.Int63n(TIMEOUTVALUEMAX - TIMEOUTVALUEMIN) + TIMEOUTVALUEMIN
				if debug { fmt.Println("Value of timeout", timeoutSlave) }
				timeout := time.NewTimer(time.Duration(timeoutSlave) * time.Millisecond)
				var newmaster bool = false
				for {
					select {
						case <- masterAlive:
							if debug { fmt.Println("Master is alive ")}
							timeout.Reset(time.Duration(timeoutSlave) * time.Millisecond)
						case <- acceptFirst:
							if debug { fmt.Println("Received election message ")}
							state = ACCEPT
						case <- masterupForSlave:
							if debug { fmt.Println("Received masterup message ")}
							newmaster = true
						case <- timeout.C:
							if debug { fmt.Println("Master died ")}
							state = CANDIDATE
							break
					}
					if !strings.EqualFold(state, SLAVE) || newmaster {
						break
					}
				}
				break
			case CANDIDATE:
				if info { fmt.Println("In state CANDIDATE") }
				acceptch = make(chan bool)
				electionch = make(chan bool)
				mutexPeerList.Lock()
				numOfPeers := peerList.Len()
				mutexPeerList.Unlock()
				go election(peerList)
				runtime.Gosched()
				timeout := time.NewTimer(time.Duration(TIMEOUTCANDIDATE) * time.Millisecond)
				num := 0
				for {
					select {
						case <- acceptch:
							if debug { fmt.Println("Received an accept message ")}
							num++
							timeout.Reset(time.Duration(TIMEOUTCANDIDATE) * time.Millisecond)
						case <- electionch:
							if debug { fmt.Println("Received election message ")}
							state = SLAVE
						case <- timeout.C:
							if debug { fmt.Println("Received all accept messages ")}
							state = MASTER
							break
					}
					if !strings.EqualFold(state, CANDIDATE) {
						break
					}
					if num == numOfPeers {
						state = MASTER
						break
					}
				}
				slaveupForMasterup = make(chan bool, numOfPeers)
				if strings.EqualFold(state, MASTER) {
					numOfPeers = masterUp(peerList)	
					if debug { fmt.Println("Number of peers is", numOfPeers) }
				}
				timeout = time.NewTimer(time.Duration(TIMEOUTCANDIDATE) * time.Millisecond)
				ch := make(chan string)
				go func() {
					for i:= 0; i < numOfPeers ; i++ {
						<- slaveupForMasterup
					}
					ch <- "Finished"
				}()
				select {
					case <- ch:
						if debug { fmt.Println("All Slaveup have been received!") }
					case <- timeout.C:
						if debug { fmt.Println("Some of Slaveup have been received!") }
				}
				masterSetUpToServer()
				break
			case ACCEPT:
				if info { fmt.Println("In state ACCEPT") }
				masterupForAccept = make(chan bool)
				timeout := time.NewTimer(time.Duration(TIMEOUTREQUESTTIME) * time.Millisecond)
						
				select {
					case <- masterupForAccept:
						if debug { fmt.Println("Received masterup message") }
						break
					case <- timeout.C:
						if debug { fmt.Println("Timeout") }
						break
				}
				state = SLAVE				
		}
	}
}
func election(peerList *list.List) {
	
	requestMsg := ELECTION + ":" + strconv.FormatUint(uint64(clientId), 16)
	if info { fmt.Println("Sending election message to slaves") }
	if debug { fmt.Println("The message is", requestMsg) }
	mutexPeerList.Lock()
	for e := peerList.Front(); e != nil; e = e.Next() {
		conn := e.Value.(*Peer).conn
		_, err := conn.Write([]byte(requestMsg)) //transmit string as byte array
		if err != nil {
			fmt.Println("Error sending reply:", err.Error())
		}
	}
	mutexPeerList.Unlock()
}


func masterUp(peerList *list.List) int {
	
	requestMsg := MASTERUP + ":" + strconv.FormatUint(uint64(clientId), 16)
	if info { fmt.Println("Sending masterup message to slaves") }
	if debug { fmt.Println("The message is", requestMsg) }
	mutexPeerList.Lock()
	numofpeers := peerList.Len()
	for e := peerList.Front(); e != nil; e = e.Next() {
		conn := e.Value.(*Peer).conn
		_, err := conn.Write([]byte(requestMsg)) //transmit string as byte array
		if err != nil {
			fmt.Println("Error sending reply:", err.Error())
		}
	}
	mutexPeerList.Unlock()
	return numofpeers
}

func requestTime(peerList *list.List, sequenceId uint64) {
	
	requestMsg := REQUESTTIME + ":" + strconv.FormatUint(uint64(clientId), 16) + ":" + strconv.FormatUint(sequenceId,10)
	if info { fmt.Println("Sending request to slaves to request their local time at time") }
	if debug { fmt.Println("The message is", requestMsg) }
	mutexPeerList.Lock()
	for e := peerList.Front(); e != nil; e = e.Next() {
		conn := e.Value.(*Peer).conn
		slaveId := hash(e.Value.(*Peer).ip + ":" + e.Value.(*Peer).port)
		slaveIdString := strconv.FormatUint(uint64(slaveId), 16)
		deltaMap[slaveIdString] = &Timedelta{}
		deltaMap[slaveIdString].atSend = localTime
		if debug { fmt.Println("Sending time request to", e.Value.(*Peer).ip + ":" + e.Value.(*Peer).port) }
		_, err := conn.Write([]byte(requestMsg)) //transmit string as byte array
		if err != nil {
			fmt.Println("Error sending reply:", err.Error())
		}
	}
	mutexPeerList.Unlock()
	
}

func correctTime(peerList *list.List, sequenceId uint64, average int64) {
		
	mutexPeerList.Lock()
	for e := peerList.Front(); e != nil; e = e.Next() {
		slaveId := hash(e.Value.(*Peer).ip + ":" + e.Value.(*Peer).port)
		slaveIdString := strconv.FormatUint(uint64(slaveId), 16)
		
		timeDifference := deltaMap[slaveIdString].delta - average
		requestMsg := CORRECTTIME + ":" + strconv.FormatUint(uint64(clientId), 10) + ":" + strconv.FormatUint(sequenceId,10) + ":" + strconv.FormatInt(timeDifference, 10)
	
		conn := e.Value.(*Peer).conn
		_, err := conn.Write([]byte(requestMsg)) //transmit string as byte array
		if err != nil {
			fmt.Println("Error sending reply:", err.Error())
		}
	}
	mutexPeerList.Unlock()
	
}

// hash function
func hash(key string) uint32 {
    if  len(key) < 64 {
		var scratch [64]byte
		copy(scratch[:], key)
		return crc32.ChecksumIEEE(scratch[:len(key)])
	}
	return crc32.ChecksumIEEE([]byte(key))
}


/**
https://github.com/mshahriarinia/Golang/blob/master/p2pChat/src/node.go
Determine the local IP addresses
*/
func chatSay(peerList *list.List) {
	reader := bufio.NewReader(os.Stdin) //get teh reader to read lines from standard input

	//conn, err := net.Dial("tcp", serverIP+":"+SERVER_PORT)

	for !stop { //keep reading inputs forever
		fmt.Print("user@Home[\\ ")
		str, _ := reader.ReadString('\n')

		mutexPeerList.Lock()
		for e := peerList.Front(); e != nil; e = e.Next() {
			conn := e.Value.(*Peer).conn
			_, err := conn.Write([]byte(str)) //transmit string as byte array
			if err != nil {
				fmt.Println("Error sending reply:", err.Error())
			}
		}
		mutexPeerList.Unlock()
	}
}
func connectToPeers(peer Peer, controlMessage string, peerList *list.List) {
	
	strArr := strings.Split(controlMessage, " ")
	if debug { fmt.Println("Connecting to peers", strArr) }
	for i, ipport := range strArr {
		if i == 0 {
			//skip preamble
		} else if i ==1 { //set actual port for the peer sending this message			
			peer.port = ipport
		}else if !isSelf(ipport) { //skip preamble
			connectToIpPort(ipport, peerList)
		}
	}
}
func connectToIpPort(ipport string, peerList *list.List) {
	if strings.Contains(ipport, "nil"){
		return
	}
	if len(strings.Trim(ipport, " ")) == 0{
		return
	}
	
	if isAlreadyconnected(ipport, peerList){
	  	return
	}
	  
	mutexPeerList.Lock()
	conn, err := net.Dial("tcp", ipport)	
	if debug { fmt.Println("Connecting to ", ipport) } 
	if err != nil {
		fmt.Println("Error connecting to:", ipport, err.Error())
		mutexPeerList.Unlock()
		return
		
	}
	peer := &Peer{conn, "nilport", getIP(conn)}
	
	//peerList.PushBack(peer)
	addToList(*peer, peerList)
	mutexPeerList.Unlock()
	
	go handlePeer(peer, peerList)
	runtime.Gosched()
}
func acceptPeers(port string, peerList *list.List) {
	if debug { fmt.Println("Listenning to port", port) } 
	ln, err := net.Listen("tcp", ":"+port)
	if err != nil {
		fmt.Println("Error listenning to port ", port)
		stop = true
	}
	for !stop {
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println("Error in accepting connection.")
			stop = true
			continue
		}
		
		mutexPeerList.Lock()
		peer := &Peer{conn, "nilport", getIP(conn)}  
		peerList.PushBack(peer)
		mutexPeerList.Unlock()
		go handlePeer(peer, peerList)
		runtime.Gosched()
	}
}
func handlePeer(peer *Peer, peerList *list.List) {
	stopConn := false
	if info { fmt.Println("New node: ", peer.conn.RemoteAddr()) }
				
	//send current peer list if I am a master
	str := ""
	if strings.EqualFold(state, MASTER) {
		str = peerListToStr(peerList) 
	} 
	
	_, err := peer.conn.Write([]byte(PEERLIST + " " + port + " " + str)) //transmit string as byte array
	fmt.Println(PEERLIST + " " + port + " " + str)
	if err != nil {
		fmt.Println("Error sending reply:", err.Error())
	}	
	
	//Listen for the peer messages
	buffer := make([]byte, BUFFERSIZE)

	for !stopConn {
		bytesRead, err := peer.conn.Read(buffer)
		if err != nil { //stop for loop, remove peer from list
			
			stopConn = true
			fmt.Println("Error in reading from connection", peer.conn.RemoteAddr())
			mutexPeerList.Lock()
			el := getListElement(*peer, peerList)
			if el != nil {
				peerList.Remove(el)
			}
			mutexPeerList.Unlock()			
		} else {
			messageStr := string(buffer[0:bytesRead])			
			if strings.Contains(messageStr, PEERLIST) {
				//pass peer itself to set actual port
				sArr := strings.Split(messageStr, " ")
				if info { fmt.Println("The port is: ", sArr[1]) }
				if debug { fmt.Println("The message is ", messageStr) }
				
				el := getListElement(*peer, peerList)
				if el != nil {
					p := el.Value.(*Peer)
					if debug { fmt.Println(p.port) }
					p.port = sArr[1]
					//peer.port = sArr[1]  
					if info { fmt.Println("Setted port to", p.port) }
					setPort(*peer, peerList, sArr[1])
					
					connectToPeers(*peer, messageStr, peerList) 
				if debug {	printlist(peerList)	}			
				}
				
			} else if strings.Contains(messageStr, REQUESTTIME) {
				// SLAVE STATE
				if info { fmt.Println("Master request my local time : ", messageStr ) }
				if debug { fmt.Println("My state should be SLAVE at this point, actual state is", state)}
				masterAlive <- true
				tokens := strings.Split(messageStr, ":")
				mutexLocalTime.Lock()
				replyBuf := []byte(LOCALTIME + ":"+strconv.FormatUint(uint64(clientId), 16) + ":" + tokens[2] + ":" + strconv.FormatUint(localTime, 10))
				mutexLocalTime.Unlock()
				_, err := peer.conn.Write(replyBuf)
				if err != nil {
					fmt.Println("Error sending reply:", err.Error())
				}					
			} else if strings.Contains(messageStr, LOCALTIME) {
				// MASTER STATE
				if info { fmt.Println("Received a slave's local time : ", messageStr ) }
				if debug { fmt.Println("My state should be MASTER at this point, actual state is", state)}
				tokens := strings.Split(messageStr, ":")
				sequenceIdRecieved, err := strconv.ParseUint(tokens[2], 10, 64)
				checkError(err, "Cannot parse string")
				if debug { fmt.Println(sequenceId, sequenceIdRecieved) }
				if sequenceId == sequenceIdRecieved {
					deltaMap[tokens[1]].atReceive = localTime
					deltaMap[tokens[1]].atSlave, err = strconv.ParseUint(tokens[3], 10, 64)
					sem <- 1
				}
				
			} else if strings.Contains(messageStr, CORRECTTIME) {
				// SLAVE STATE
				if info { fmt.Println("Master correct my local time : ", messageStr ) }
				if debug { fmt.Println("My state should be SLAVE at this point, actual state is", state)}
				masterAlive <- true
				tokens := strings.Split(messageStr, ":")
				correctTime, err := strconv.ParseInt(tokens[3], 10, 64)
				checkError(err, "Can't parseuint")
				mutexLocalTime.Lock()
				if correctTime > 0 {
					localTime = localTime + uint64(correctTime)
				} else {
					localTime = localTime - uint64(-correctTime)
				}	
				mutexLocalTime.Unlock()
			} else if strings.Contains(messageStr, ELECTION){
				switch state {
					case SLAVE:
						replyBuf := []byte(ACCEPT + ":"+strconv.FormatUint(uint64(clientId), 16))
						
						_, err := peer.conn.Write(replyBuf)
						if err != nil {
							fmt.Println("Error sending reply:", err.Error())
						}		
						acceptFirst <- true			
					case CANDIDATE:
						replyBuf := []byte(REFUSE + ":"+strconv.FormatUint(uint64(clientId), 16))
						
						_, err := peer.conn.Write(replyBuf)
						if err != nil {
							fmt.Println("Error sending reply:", err.Error())
						}		
					case ACCEPT:
						replyBuf := []byte(REFUSE + ":"+strconv.FormatUint(uint64(clientId), 16))
						
						_, err := peer.conn.Write(replyBuf)
						if err != nil {
							fmt.Println("Error sending reply:", err.Error())
						}		
					default:
						fmt.Println("Fatal error, should not reach here when received a election message. My current state is", state )
				}
			} else if strings.Contains(messageStr, ACCEPT) {
				switch state {		
					case CANDIDATE:
						acceptch <- true
					default:
						fmt.Println("Fatal error, should not reach here when received a accept message. My current state is", state )
				}
			
			} else if strings.Contains(messageStr, REFUSE) {
				switch state {		
					case CANDIDATE:
						electionch <- true
					default:
						fmt.Println("Fatal error, should not reach here when received a refuse message. My current state is", state)
				}
			} else if strings.Contains(messageStr, MASTERUP) {
				switch state {		
					case ACCEPT:
						if debug { fmt.Println("In state ACCEPT, received Masterup request") }
						replyBuf := []byte(SLAVEUP + ":" + strconv.FormatUint(uint64(clientId), 16))
						_, err := peer.conn.Write(replyBuf)
						if err != nil {
							fmt.Println("Error sending reply:", err.Error())
						}	
						masterupForAccept <- true
						if debug { fmt.Println("masterupForAccept is true") }
					case SLAVE:
						if debug { fmt.Println("In state SLAVE, received Masterup request") }
						replyBuf := []byte(SLAVEUP + ":" + strconv.FormatUint(uint64(clientId), 16))
						_, err := peer.conn.Write(replyBuf)
						if err != nil {
							fmt.Println("Error sending reply:", err.Error())
						}	
						masterupForSlave <- true
					default:
						fmt.Println("Fatal error, should not reach here when received a masterup message. My current state is", state)
				}
				
			} else if strings.Contains(messageStr, SLAVEUP) {
				switch state {		
					case MASTER:
						if debug { fmt.Println("In state MASTER, received Slaveup response") }
						
						slaveupForMasterup <- true
					default:
						fmt.Println("Fatal error, should not reach here when you received a SLAVEUP message. My current state is", state) 
						}
			} else {
				//TODO handle images receiving shuochen
				//peer.conn.Write()
				//
				printlist(peerList)
				fmt.Println(peer.ipport(), " says: ", messageStr)
			}
		}
	}
	fmt.Println("Closing ", peer.conn.RemoteAddr())
	peer.conn.Close()
}
func setPort(peer Peer, l *list.List, port string) *list.Element {
	for e := l.Front(); e != nil; e = e.Next() {
		temp := e.Value.(*Peer)
		
		if peer.conn.RemoteAddr() == temp.conn.RemoteAddr() {
			if debug {	fmt.Println("Set port") }
			temp.port = port
			return e
		}
	}
	return nil
}

/**
return the element of the list that represents the same peer as the arguemnt
*/
func getListElement(peer Peer, l *list.List) *list.Element {
	for e := l.Front(); e != nil; e = e.Next() {
		temp := e.Value.(*Peer)
		
		if peer.conn.RemoteAddr() == temp.conn.RemoteAddr() {
			if debug { fmt.Println("Found connection.") }
			return e
		}
	}
	return nil
}



/**
Avoid adding redundant peers to list, shall be already locked by mutex
*/
func addToList(peer Peer, l *list.List)  {
	if ! isAlreadyconnected(peer.ipport(), l){			  
		l.PushBack(&peer)
	}
	return 
}

/**
check if the ipport combination is already being conneted to
*/
func isAlreadyconnected(ipport string, l *list.List)bool{
	for e := l.Front(); e != nil; e = e.Next() {
		temp := e.Value.(*Peer)		
		if ipport == temp.ipport() {
			return true
		}
	}			  
	return false
}


/**
Get a string of the peer list as ip:port
*/
func peerListToStr(l *list.List) string {
	if l == nil {
		return ""
	}
	s := ""
	mutexPeerList.Lock()
	for e := l.Front(); e != nil; e = e.Next() {
		peer := e.Value.(*Peer)
		if peer.port != "nilport"{
			s = s + peer.ip + ":" + peer.port + " "
		} else {
			
		}
	}
	//s = s + getLocalIP()[0] + ":" + port
	mutexPeerList.Unlock()
	return strings.Trim(s, " ")
}

/**
print ipport combination of the current peer list 
*/
func printlist(l *list.List) {
	fmt.Print("\nPeer List: [")
	fmt.Print(peerListToStr(l))
	fmt.Println("]")
}

/**
struct function to return the ipport combination to be used for comparisons
*/
func (p *Peer) ipport() string{
	return p.ip + ":" + p.port
}

/**
Checks to see if the ipport combination is the current node itself. 
*/
func isSelf(ipport string) bool {
	ipArr := getLocalIP()

	for _, ip := range ipArr {
		if ipport == ip+":"+port {
			return true
		}
	}
	if ipport == "127.0.0.1"+":"+port || ipport == "localhost"+":"+port {
		return true
	}
	return false
}

/**
Generate a port number
*/
func generatePortNo() string {
	rand.Seed(time.Now().Unix())
	return strconv.Itoa(rand.Intn(5000) + 5000) //generate a valid port
}

/**
return the ip address of a tcp connection
*/
func getIP(conn net.Conn) string{
	s := conn.RemoteAddr().String()
	s = strings.Split(s, ":")[0]
	s = strings.Trim(s, ":")
	return s
}


/**
Determine the local IP addresses
*/
func getLocalIP() []string {
	name, err := os.Hostname()
	if err != nil {
		fmt.Printf("Oops: %v\n", err)
		return []string{}
	}
	//fmt.Print("Local Hostname: " + name)

	addrs, err := net.LookupHost(name)
	if err != nil {
		fmt.Printf("Oops: %v\n", err)
		return []string{}
	}
	//	fmt.Print("\t\tLocal IP Addresses: ", addrs)

	return addrs
}

func checkError(err error, msg string) {
	if err != nil {
		fmt.Println(msg)
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}